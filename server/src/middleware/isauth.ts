import { MyContext } from "src/types";
import { MiddlewareFn } from "type-graphql";

/**
 * This middleware check if the user is authentificated
 * @param param0 
 * @param next 
 */
export const isAuth: MiddlewareFn<MyContext> = ({ context }, next) => {
  if (!context.req.session.userId) {
    throw new Error("not authenticated");
  }
  return next();
};
