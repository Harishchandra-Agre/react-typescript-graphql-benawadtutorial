import DataLoader from "dataloader";
import { User } from "../entities/User";

// keys is a userId table
// it returns table of user object
export const createUserLoader = () =>
  new DataLoader<number, User>(async (userIds) => {
    const users = await User.findByIds(userIds as number[]);
    // following step is needed if the order matter
    const userIdToUser: Record<number, User> = {};
    users.forEach((user) => (userIdToUser[user.id] = user));

    return userIds.map((id) => userIdToUser[id]);
  });
