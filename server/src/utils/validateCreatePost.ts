import { PostInput } from "src/resolvers/PostInput";

export const validateCreatePost = (input: PostInput) => {
  if (!input.title) {
    return [{ field: "title", message: "The post nead a title" }];
  }

  if (!input.text) {
    return [{ field: "text", message: "The post text is required" }];
  }

  if (input.title.length > 50) {
    return [
      {
        field: "title",
        message: "The post title must be less than 50 characters",
      },
    ];
  }

  return null;
};
