import { config } from "dotenv";
import { resolve } from "path";

config({ path: resolve(__dirname, "../.env") });

const __prod__ = process.env.NODE_ENV === "production";
const __db_user__ = process.env.DB_USER;
const __db_password__ = process.env.DB_PASSWORD;
const __redis_password__ = process.env.REDIS_PASSWORD;
const __ethereal_user__ = process.env.ETHEREAL_USER;
const __ethereal_password__ = process.env.ETHEREAL_PASSWORD;
const COOKIE_NAME = "qid";
const FORGET_PASSWORD_PREFIX = "forget-password:";
const __web_url__ = process.env.WEB_URL;

export {
  __prod__,
  __db_user__,
  __db_password__,
  __redis_password__,
  COOKIE_NAME,
  FORGET_PASSWORD_PREFIX,
  __ethereal_user__,
  __ethereal_password__,
  __web_url__,
};
