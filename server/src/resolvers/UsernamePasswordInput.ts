import {
  InputType,
  Field
} from "type-graphql";

// Use as custom argument type for query and mutation

@InputType()
export class UsernamePasswordInput {
  @Field()
  username: string;
  @Field()
  email: string;
  @Field()
  password: string;
}
