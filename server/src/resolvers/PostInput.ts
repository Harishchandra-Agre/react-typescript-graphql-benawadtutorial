import { InputType, Field } from "type-graphql";

// Use as custom argument type for query and mutation

@InputType()
export class PostInput {
  @Field()
  title: string;
  @Field()
  text: string;
}
