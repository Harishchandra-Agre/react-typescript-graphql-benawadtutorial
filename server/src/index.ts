import { ApolloServer } from "apollo-server-express";
import connectRedis from "connect-redis";
import cors from "cors";
import express from "express";
import session from "express-session";
import Redis from "ioredis";
import "reflect-metadata";
import { buildSchema } from "type-graphql";
import {
  COOKIE_NAME,
  __prod__,
  __redis_password__,
  __db_user__,
  __db_password__,
} from "./constants";
import { HelloResolver } from "./resolvers/hello";
import { PostResolver } from "./resolvers/post";
import { UserResolver } from "./resolvers/user";
import { createConnection } from "typeorm";
import { Post } from "./entities/Posts";
import { User } from "./entities/User";
import path from "path";
import { Updoot } from "./entities/Updoot";
import { createUserLoader } from "./utils/createUserLoader";
import { createUpdootLoader } from "./utils/createUpdootLoader";

const main = async () => {
  // create a connection to the database
  const conn = await createConnection({
    type: "postgres",
    database: "lireddit2",
    username: __db_user__,
    password: __db_password__,
    logging: true,
    synchronize: true, // automatically create the tables without need of migration
    migrations: [path.join(__dirname, "./migrations/*")],
    entities: [Post, User, Updoot],
  });

  await conn.runMigrations();

  // await Post.delete({});

  const app = express();

  //set up redis and store session object in redis store
  const RedisStore = connectRedis(session);
  const redis = new Redis({ password: __redis_password__ || "" });

  //set up cors with express cors middleware
  app.use(cors({ origin: "http://localhost:3000", credentials: true }));
  // set up session middleware
  app.use(
    session({
      name: COOKIE_NAME,
      store: new RedisStore({ client: redis, disableTouch: true }),
      cookie: {
        maxAge: 1000 * 60 * 60 * 24 * 365 * 10, // 10 years
        httpOnly: true,
        sameSite: "lax", // csrf ( cross-site request forgery)
        secure: __prod__, // cookie only works in https
      },
      saveUninitialized: false,
      secret: "dsonlpjmzdinojnponpo",
      resave: false,
    })
  );

  // set up apollo middleware
  // context is a special object that is accessible by all the resolvers
  const apolloServer = new ApolloServer({
    schema: await buildSchema({
      resolvers: [HelloResolver, PostResolver, UserResolver],
      validate: false,
    }),
    context: ({ req, res }) => ({
      req,
      res,
      redis,
      userLoader: createUserLoader(),
      updootLoader: createUpdootLoader(),
    }),
  });

  // create a /graphql endpoint on express
  apolloServer.applyMiddleware({
    app,
    cors: false,
  });
  // the backend will listen to all request on port 4000
  app.listen(4000, () => {
    console.log("server started on port 4000");
  });
};

main().catch((err) => {
  console.log(err);
});
