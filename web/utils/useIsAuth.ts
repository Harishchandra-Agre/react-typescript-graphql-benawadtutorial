import { useRouter } from "next/router";
import { useEffect } from "react";
import { useMeQuery } from "../src/generated/graphql";

export const useIsAuth = () => {
  const [{ data, fetching }] = useMeQuery();
  const router = useRouter();

  // if it's not fetching and there is no connected user, redirect to login page
  useEffect(() => {
    if (!fetching && !data?.me) {
      // this part '"?next=" + router.pathname' will route the user to the current path after the user logged in
      router.replace("/login?next=" + router.pathname);
    }
  }, [fetching, data, router]);
};
