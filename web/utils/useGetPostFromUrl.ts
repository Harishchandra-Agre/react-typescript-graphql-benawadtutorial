import { usePostQuery } from "../src/generated/graphql";

export const useGetPostFromUrl = (id: number) => {
  return usePostQuery({
    pause: id === -1,
    variables: {
      id,
    },
  });
};
